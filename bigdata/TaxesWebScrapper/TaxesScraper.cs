﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using ScrapySharp.Extensions;
using ScrapySharp.Network;

namespace TaxesWebScrapper
{
    class TaxesScraper
    {
        private const string UrlTemplate = @"http://www.portal.nalog.gov.by/debtor/#!ul;page={0}";

        private static readonly HttpClient client_ = new HttpClient { Timeout = new TimeSpan(0, 1, 0) };

        public TaxesScraper()
        {
            
        }

        public async void Run()
        {
            await LoadData();
        }

        public async Task LoadData()
        {
            const int MaxPageNumber = 5; //4211

            var browser = new ScrapingBrowser();
            browser.AllowAutoRedirect = true;
            browser.AllowMetaRedirect = true;
            
            var result = new List<HtmlNode>();

            for (var i = 1; i < MaxPageNumber; i++)
            {
                var pageUrl = string.Format(UrlTemplate, i);
                var page = await LoadDocument(pageUrl);
                //var html = page.;
                //var trNodes = html.CssSelect("tr");
                //result.AddRange(trNodes.Where(_ => _.HasClass(".b-h")));
            }
        }

        private static async Task<HtmlDocument> LoadDocument(string url)
        {
            for (var index = 0; index < 3; index++)
            {
                try
                {
                    var content = await (await client_.GetAsync(url)).Content.ReadAsStringAsync();
                    var htmlDoc = new HtmlDocument();
                    htmlDoc.LoadHtml(content);

                    return htmlDoc;
                }
                catch (Exception e)
                {
                    await Task.Delay(new TimeSpan(0, 5, 0));
                }
            }

            throw new Exception("Can not load page.");
        }
    }
}
