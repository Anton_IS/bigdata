﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TaxesWebScrapper
{
    class Program
    {
        static void Main(string[] args)
        {
            var scraper = new TaxesScraper();
            scraper.Run();

            Console.ReadKey();
        }
    }
}
