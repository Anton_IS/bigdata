﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TaxesWebScrapper
{
    public class TaxRecord
    {
        public int Number { get; set; }
        public int Unp { get; set; }
        public string FullName { get; set; }
        public string Inspection { get; set; }
        public string DebtDateString { get; set; }
        public string RepaymentDateString { get; set; }
    }
}
